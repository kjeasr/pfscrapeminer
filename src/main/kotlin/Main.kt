import com.fasterxml.jackson.databind.ObjectMapper
import com.google.gson.Gson
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*


fun main(args: Array<String>) {

    val startLon = BigDecimal(args[1])
    val endLon = BigDecimal(args[2])
    val startLat = BigDecimal(args[3])
    val endLat = BigDecimal(args[4])

    if (args[0].equals("-f", true)) {
        val apiKey = args[5]
        val sharedSecret = args[6]
        var from: Date? = null
        var to: Date? = null
        var allPages = false
        var searchSize = "0.0001"
        val dateFormatter = SimpleDateFormat("yyyy-mm-dd")

        for (arg in args) {
            if (arg.startsWith("-from="))
                from = dateFormatter.parse(arg.split("=")[1])
            if (arg.startsWith("-to="))
                to = dateFormatter.parse(arg.split("=")[1])
            if (arg.startsWith("-allPages"))
                allPages = true
            if (arg.startsWith("-ss"))
                searchSize = arg.split("=")[1]
        }
        val result = flickr.scanArea(startLon, startLat, endLon, endLat, apiKey, sharedSecret, from, to, allPages, searchSize)
        val gson = Gson()
        println(gson.toJson(result))
    } else if (args[0].equals("-p", true)) {
        var searchSize = "0.1"
        var picture = true
        for (arg in args) {
            if (arg.startsWith("-ss=")) {
                searchSize = arg.split("=")[1]
            } else if (arg.startsWith("-noPicture"))
                picture = false
        }
        val result = pokemon.scanArea(startLon, endLon, startLat, endLat, searchSize, picture)
        val objectMapper = ObjectMapper()
        println(objectMapper.writeValueAsString(result))
    } else {
        error("Dont know where to search")
    }
}

