package flickr

import com.flickr4java.flickr.photos.Photo
import com.flickr4java.flickr.photos.PhotoList

class FlickrPageWrapper(var startLongitude: String, var endLongitude: String, var startLatitude: String,
                        var endLatitude: String, var result: PhotoList<Photo>?, var startDate: java.util.Date? = null,
                        var endDate: java.util.Date? = null)
