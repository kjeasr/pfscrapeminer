package flickr
import com.flickr4java.flickr.REST
import com.flickr4java.flickr.Flickr
import com.flickr4java.flickr.photos.SearchParameters
import java.math.BigDecimal
import java.util.*

var apiKey = "e884a95b9ebcdb1a38d9413ce0733fe9"
var sharedSecret = "YOUR_SHARED_SECRET"

// start: 59.434177, 24.734417
// end: 59.438761, 24.743343

// 60.165067, 24.947166 60.171596, 24.955511

fun main(args: Array<String>) {
    scanArea(BigDecimal("24.947166"), BigDecimal("24.955511"), BigDecimal("60.165067"), BigDecimal("60.171596"), apiKey, sharedSecret
            //,java.sql.Date.valueOf(LocalDate.of(2017, Month.MAY, 1)), java.sql.Date.valueOf(LocalDate.of(2017, Month.MAY, 19)))
    )
}

fun scanArea(startLon: BigDecimal, startLat: BigDecimal, endLon: BigDecimal, endLat: BigDecimal, apiKey: String = "",
             sharedSecret: String = "", from: Date? = null, to: Date? = null, allPages: Boolean = false,
             searchSize: String = "0.0001"/*"0.00001"*/): MutableList<FlickrPageWrapper> {

    val flickr = Flickr(apiKey, sharedSecret, REST())
    val photosInterface = flickr.photosInterface
    val searchParameters = SearchParameters()
    val result: MutableList<FlickrPageWrapper> = mutableListOf()

    if (from != null) {
        searchParameters.minTakenDate = from
    }
    if (to != null) {
        searchParameters.maxTakenDate = to
    }

    var latitude = startLat
    var longitude: BigDecimal
    var newLongitude: BigDecimal
    while (latitude < endLat) {
        longitude = startLon

        while (longitude < endLon) {
            newLongitude = longitude + BigDecimal(searchSize)
            val endLatitude = (latitude + BigDecimal(searchSize))

            searchParameters.setBBox(longitude.toString(), latitude.toString(), newLongitude.toString(), endLatitude.toString())
            var photoList = photosInterface.search(searchParameters, 250, 1)

            result.add(FlickrPageWrapper(longitude.toString(), newLongitude.toString(), latitude.toString(), endLatitude.toString(), photoList, from, to))

            if (allPages && photoList.pages > 1) {
                for (i in 2..photoList.pages) {
                    photoList = photosInterface.search(searchParameters, 250, i)
                    result.add(FlickrPageWrapper(longitude.toString(), newLongitude.toString(), latitude.toString(), endLatitude.toString(), photoList, from, to))
                }
            }

            longitude = newLongitude
        }

        latitude += BigDecimal(searchSize)
    }
    return result
}

