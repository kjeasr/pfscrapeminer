package pokemon

import java.math.BigDecimal

class PokeStop {
    var longitude: BigDecimal? = null
    var latitude: BigDecimal? = null
    // 0 = valorGym, 1 = pokeStop, 2 = instantGym
    var type: Int = 0
    var unknown1: Int = 0
    var unknown2: Int = 0
    var id: Int = 0
    var tag: String = ""
    var name: String = ""
    var picture: String = ""
}
