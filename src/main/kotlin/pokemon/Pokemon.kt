package pokemon

import khttp.get
import khttp.post
import khttp.responses.Response
import java.math.BigDecimal
import java.math.MathContext
import java.util.*

var phpSessid: String = ""

val HOST = "www.pokemo" + "n" + "g" + "omap.info"
val SITE = "http://" + HOST
val HTTPS_SITE = "https://www.pokemo" + "n" + "g" + "omap.info"
val LOC = HTTPS_SITE + "/includes/locdata.php"
val DATA_URL = HTTPS_SITE + "/includes/uy2" + "2ew" + "sd1.php"

fun main(args: Array<String>) {
    scanArea(BigDecimal("24.655942"), BigDecimal("24.677322"), BigDecimal("59.390969"), BigDecimal("59.399242"))
}

/**
 * default box size is about 11.1 km
 * Please use with caution
 */
fun scanArea(startLon: BigDecimal, endLon: BigDecimal, startLat: BigDecimal, endLat: BigDecimal,
             searchSize: String = "0.1", getPicture: Boolean = true): MutableList<PokeStop> {
    val result = mutableListOf<PokeStop>()

    if (phpSessid.isEmpty()) {
        registerSessionId()
    }

    val cookie = "PHPSESSID=$phpSessid"

    var latitude = startLat
    var longitude: BigDecimal
    var newLongitude: BigDecimal?
    while (latitude < endLat) {

        longitude = startLon
        while (longitude < endLon) {

            newLongitude = (longitude + BigDecimal(searchSize))
            val endLatitude = (latitude + BigDecimal(searchSize))
/*
            Site does not seam to use zoom feature from header
            val cookieLat = (latitude + endLatitude).divide(BigDecimal(2), MathContext(10))
            val cookieLon = (longitude + newLongitude).divide(BigDecimal(2), MathContext(10))
            var reqCookie = "$cookie announcement=1; latlngzoom=15[##split##]$cookieLat[##split##]$cookieLon"
*/

            // Spam protection
            Thread.sleep(2500)
            do {
                var ok = false
                val locationRequest = makeRequest(cookie, latitude, endLatitude, longitude, newLongitude)

                val responseText = locationRequest.text
                // check if there are points of interest
                if (responseText != "[]") {
                    val responseJson = locationRequest.jsonObject
                    if (responseJson.has("spamtime")) {
                        val timeout = (responseJson.get("spamtime") as Int).toLong() + 3
                        Thread.sleep(timeout * 1000)
                    } else {
                        val pokeStops = parser(responseText, getPicture)
                        result.addAll(pokeStops)
                        ok = true
                    }
                } else {
                    ok = true
                }


            } while(!ok)

            longitude = newLongitude
        }
        latitude += BigDecimal(searchSize)
    }
    return result
}

fun makeRequest(cookie: String, latitude: BigDecimal, endLatitude: BigDecimal, longitude: BigDecimal, newLongitude: BigDecimal): Response {
    return post(DATA_URL,
            mapOf("cookie" to cookie,
                    //"User-Agent" to "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0",
                    "Content-Type" to "application/x-www-form-urlencoded; charset=UTF-8",
                    "Host" to HOST,
                    "Origin" to HTTPS_SITE,
                    "Referer" to HTTPS_SITE+"/",
                    "X-Requested-With" to "XMLHttpRequest")
            ,mapOf(),
            mapOf(
                    "fromlat" to latitude,
                    "tolat" to endLatitude,
                    "fromlng" to longitude,
                    "tolng" to newLongitude,
                    "fpoke" to "1",
                    "fgym" to "1",
                    "farm" to "0"
            ))
}

fun registerSessionId() {
    val poke = get(SITE)
    phpSessid = poke.cookies["PHPSESSID"]?.split(";")?.get(0)!!
}

fun parser(text: String, getPicture: Boolean = false): MutableList<PokeStop> {
    val H = BigDecimal(1.852) // magic number found from obfuscated code
    val comas = BigDecimal(1e6) // correcting coordinates
    val values = text.subSequence(1, (text.length - 2)).split("},")
    val decoder = Base64.getDecoder()
    val results: MutableList<PokeStop> = mutableListOf()
    values.forEach { it ->
        val entireItem = it.split(":{")
        val id = entireItem[0].replace("\"", "")
        val item = entireItem[1].split(",")
        val result = PokeStop()
        item.forEachIndexed { index, field ->
            val kv = field.split(":")
            when (index) {
                0 -> result.longitude = BigDecimal(String(decoder.decode(kv[1].replace("\"", ""))).split("\\.")[0]).divide(H, MathContext(10)).divide(comas, MathContext(10))
                1 -> result.latitude = BigDecimal(String(decoder.decode(kv[1].replace("\"", ""))).split("\\.")[0]).divide(H, MathContext(10)).divide(comas, MathContext(10))
                2 -> result.type = String(decoder.decode(kv[1].replace("\"", ""))).toInt()
                3 -> result.unknown1 = String(decoder.decode(kv[1].replace("\"", ""))).toInt()
                4 -> result.unknown2 = String(decoder.decode(kv[1].replace("\"", ""))).toInt()
                5 -> result.id = String(decoder.decode(kv[1].replace("\"", ""))).toInt()
                6 -> result.tag = kv[1].replace("\"", "")
                7 -> result.name = kv[1].replace("\"", "")
            }
        }
        if (id != result.id.toString()) {
            error("$id does not match item")
        }
        if (getPicture)
            getPicture(result)
        results.add(result)
    }
    return results
}

fun getPicture(pokeStop: PokeStop) {
    if (phpSessid.isEmpty()) {
        registerSessionId()
    }

    val cookie = "PHPSESSID=$phpSessid"

    val locationRequest = post(LOC,
            mapOf("cookie" to cookie,
                    "User-Agent" to "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0",
                    "Content-Type" to "application/x-www-form-urlencoded; charset=UTF-8",
                    "Host" to HOST,
                    //"Accept" to "application/json, text/javascript, */*; q=0.01",
                    //"Accept Encoding" to "gzip, deflate, br",
                    "Origin" to HTTPS_SITE,
                    "Referer" to HTTPS_SITE+"/",
                    "X-Requested-With" to "XMLHttpRequest")
            ,mapOf(),
            mapOf(
                    "mid" to pokeStop.id
            ))

    pokeStop.picture = locationRequest.jsonObject["poke_image"] as String
}
