import pokemon.getPicture
import pokemon.parser
import com.flickr4java.flickr.Flickr
import com.flickr4java.flickr.REST
import com.flickr4java.flickr.photos.SearchParameters
import flickr.apiKey
import flickr.sharedSecret
import khttp.get
import khttp.post
import org.junit.Ignore
import org.junit.Test
import java.time.LocalDate
import java.time.Month

class CodeSnipets {
    @Test
    @Ignore
    fun makeRequestFlickr() {
        //val f = Flickr(Flickr.getApiKey, Flickr.getSharedSecret, REST())
        val f = Flickr(apiKey, sharedSecret, REST())
        val testInterface = f.testInterface
        val results = testInterface.echo(mapOf())
        val searchInterface = f.photosInterface
        val searchParameters = SearchParameters()
        searchParameters.latitude = "59.4370"
        searchParameters.longitude = "24.7536"
        searchParameters.hasGeo = true
        searchParameters.radius = 1
        searchParameters.minTakenDate = java.sql.Date.valueOf(LocalDate.of(2017, Month.MAY, 1))
        //searchParameters.maxTakenDate = java.sql.Date.valueOf(LocalDate.of(2017, Month.MAY, 19))
        //searchParameters.setBBox("59.4370", "24.7536", "60.5370", "25.8536")
        val realResult = searchInterface.search(searchParameters, 10000, 2)
        val total = realResult.total
        realResult
                .filter { it.title != "" }
                .forEach { println(it.title) }
        println(total)
    }
}