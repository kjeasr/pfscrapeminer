# Data miner for Flickr and pokemon go

 to build jar file use gradle 3.5 or gradle wrapper in the project:

 ```
 gradle build
 ```

 resulted jar is in folder
 
 ```
 build/libs
 ```
 
 ### Mining Flickr
 
 for collecting data in flickr
 
 ```
 java -jar final-1.0-SNAPSHOT.jar -f <start longitude> <end longitude> <start latitude> <end latitude>
 <apiKey> <apiSharedSecret> <optional: from the date to start the search -from=2017-05-26>
 <optional: to what the date to start the search -from=2017-05-26> <optional: allPages scan all awailable pages -allPages>
 <opitonal: size of the search box -ss=0.0001>
 ```
 
 example with minimum parameters
 
 ```
 java -jar final-1.0-SNAPSHOT.jar -f 24.664225 24.671654 59.391616 59.397297 e884a95b9ebcdb1a38d9413ce0733fe9 secret
 ```
 ### Mining pokemon go 
 
 this also contains anti spam strategy 2.5 sec between requests and timeout waiter
 
 for collecting data in pokemon go 
 
 ```
 java -jar final-1.0-SNAPSHOT.jar -p <start longitude> <end longitude> <start latitude> <end latitude> 
 <opitonal: size of the search box -ss=0.1> <opitonal: not gather stops metadata including img URL -noPicture>  
 ```
 
 example with all parameters
 ```
 java -jar final-1.0-SNAPSHOT.jar -p 24.596762 24.901727 59.355805 59.496325 -ss=0.1 -noPicture  
 ``` 
 